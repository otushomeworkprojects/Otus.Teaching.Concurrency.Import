using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public static class Extensions
    {
        public static IEnumerable<List<Customer>> Partition(this List<Customer> values, int chunkSize)
        {
            for (int i = 0; i < values.Count; i += chunkSize)
            {
                yield return values.GetRange(i, Math.Min(chunkSize, values.Count - i));
            }
        }
    }
    public class MultiThreadDataLoader: IDataLoader
    {
        private List<Customer> _customerList;
        private CountdownEvent cde = new CountdownEvent(4);
        public MultiThreadDataLoader(List<Customer> customers) { _customerList = customers; }
        public void LoadData()
        {
            var parts = Extensions.Partition(_customerList, 250000);

            Console.WriteLine("Loading data...");

            foreach (var part in parts)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(SavePatchToDB), part);
            }
            cde.Wait();
            Console.WriteLine("Loaded data...");
        }

        private void SavePatchToDB(object? obj)
        {
            try
            {
                var count = 5;
                var customers = obj as List<Customer>;
                var repository = new CustomerRepository();
                foreach (var customer in customers)
                {
                    var t = true;
                    while(t) {
                        t = !repository.AddCustomer(customer).GetAwaiter().GetResult();
                        Console.WriteLine(t);
                        count--; 
                    }
                }
                cde.Signal();
                // customers.ForEach(async c => await repository.AddCustomer(c));
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}