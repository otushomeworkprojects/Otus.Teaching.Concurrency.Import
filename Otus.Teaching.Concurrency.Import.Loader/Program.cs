﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.csv");
        
        static async Task Main(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                if (args[0].Equals("process"))
                {
                    var p = new Process();
                    p.StartInfo.FileName = "D:\\otus\\Otus.Teaching.Concurrency.Import\\Otus.Teaching.Concurrency.Import.DataGenerator.App\\bin\\Debug\\netcoreapp3.1\\Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";
                    p.StartInfo.Arguments = _dataFilePath;
                    p.Start();
                    Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");
                }
                else
                {
                    Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");
                    GenerateCustomersDataFile();
                }
            }
            else
            {
                Console.WriteLine("Argument to generate is required");
                 return;
            }

            //var parser = new XmlParser();
            var parser = new CsvParser();
            Console.WriteLine("Parsing xml to list");

            var loader = new MultiThreadDataLoader(parser.Parse());

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            loader.LoadData();

            stopwatch.Stop();

            Console.WriteLine($"Elapsed: {stopwatch.ElapsedMilliseconds}");
        }

        static void GenerateCustomersDataFile()
        {
            // var xmlGenerator = new XmlGenerator(_dataFilePath, 1000000);
            // xmlGenerator.Generate();
            var csvgenerator = new CSVGenerator(_dataFilePath, 1000000);
            csvgenerator.Generate();
        }
    }
}