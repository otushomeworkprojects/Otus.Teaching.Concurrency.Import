using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {  
        public async Task<bool> AddCustomer(Customer customer)
        {
            try
            {
                using var _dataContext = new CustomerContext();
                await _dataContext.Set<Customer>().AddAsync(customer);
                await _dataContext.SaveChangesAsync();
                await _dataContext.DisposeAsync();
                return true;
            }
            catch (Exception ex) { 
                return false; }
        }
    }
}