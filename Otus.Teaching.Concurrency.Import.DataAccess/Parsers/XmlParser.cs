﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        private static string _dataFilePath = "D:\\otus\\Otus.Teaching.Concurrency.Import\\Otus.Teaching.Concurrency.Import.Loader\\bin\\Debug\\netcoreapp3.1\\customers.xml";

        public List<Customer> Parse()
        {
            var customers = new List<Customer>();

            var file = File.ReadAllText(_dataFilePath);
            var doc = XDocument.Parse(file);

            customers = (from r in doc.Descendants("Customer")
                             select new Customer()
                             {
                                 FullName = (string)r.Element("FullName"),
                                 Email = (string)r.Element("Email"),
                                 Id = (int)r.Element("Id"),
                                 Phone = (string)r.Element("Phone"),
                             }).ToList();
            return customers;
        }
    }
}