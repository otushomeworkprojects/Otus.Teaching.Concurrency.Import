﻿using Microsoft.VisualBasic.FileIO;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<List<Customer>>
    {
        private static string _dataFilePath = "D:\\otus\\Otus.Teaching.Concurrency.Import\\Otus.Teaching.Concurrency.Import.Loader\\bin\\Debug\\netcoreapp3.1\\customers.csv";

        public List<Customer> Parse()
        {
            var customers = new List<Customer>();
            using (TextFieldParser parser = new TextFieldParser(_dataFilePath))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                string[] fields = parser.ReadFields();

                while (!parser.EndOfData)
                {
                    fields = parser.ReadFields();
                    customers.Add(new Customer
                    {
                        Id = int.Parse(fields[0]),
                        FullName = fields[1],
                        Email = fields[2],
                        Phone = fields[3]

                    });
                }
            }

            return customers;
        }
    }
}
